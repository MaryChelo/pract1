#modulename
https://docs.puppet.com/puppet/3.7/modules_documentation.html#readme-template
####Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with [Modulename]](#setup)
    * [What [Modulename] affects](#what-[modulename]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [Modulename]](#beginning-with-[Modulename])
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)
7. [Codigo](https://gitlab.com/MaryChelo/pract1/blob/master/prueba/p.py)

## Overview

The cat module installs, configures, and maintains your cat in both
apartment and residential house settings.

## Module Description

The cat module automates the installation of a cat to your apartment or
house, and then provides options for configuring the cat to fit your
environment's needs. Once installed and configured, the cat module
automates maintenance of your cat through a series of resource types and provi

## Setup

### What cat affects

* Your dog door may be overwritten if not secured before installation.
* ### Beginning with cat

Declare the main `::cat` class.

## Usage

All interaction with your cat can be done through the main cat class. With
the default options, a basic cat will be installed with no optimizations.

### I just want cat, what's the minimum I need?

    include '::cat'

### I want to configure my lasers

Use the following to configure your lasers for a random-pattern, 20 minute
playtime at 3AM local time.

    class { 'cat':
      laser => {
        pattern    => 'random',
        duration   => '20',
        start_time => '0300',
      }
    }
## Reference

### Classes

#### Public Classes
*[`pet::cat`](#petcat): Installs and configures a cat in your environment.

#### Private Classes
*[`pet::cat::install`](#petcatinstall): Handles the cat packages.
*[`pet::cat::configure`](#petcatconfigure): Handles the configuration file.